package com.example.ceofalcode.sbertech.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ceofalcode.sbertech.Constants;
import com.example.ceofalcode.sbertech.R;
import com.example.ceofalcode.sbertech.controller.MainController;

import java.util.ArrayList;

public class CurrenciesActivity extends AppCompatActivity {

    private final MainController _controller = new MainController();

    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currencies);
        layoutInitialize(getApplicationContext());
    }

    public void currencyReturn(View view) {
        Button b = (Button) view;
        String currency = b.getText().toString().substring(0, 3);
        ArrayList currencyList = _controller.getDataByName(currency, getApplicationContext());
        Intent intent = new Intent();
        intent.putExtra(Constants.EXTRA_LIST, currencyList);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void layoutInitialize(Context context) {
        linearLayout = (LinearLayout) findViewById(R.id.in_scroll_ll);
        fillListWithItems(_controller.getCurrenciesNameList(context));
    }

    private void fillListWithItems(ArrayList<String> currenciesNameList) {
        for (int i = 0; i < currenciesNameList.size(); i++) {
            addItemToList(currenciesNameList.get(i), i);
        }
    }

    private void addItemToList(String text, int id) {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout newItemLL = (LinearLayout) inflater.inflate(R.layout.scroll_view_item, null);

        newItemLL.setTag(R.id.item_tv + "_" + id);
        TextView newItemTV = (TextView) newItemLL.findViewById(R.id.item_tv);
        newItemTV.setText(text);

        linearLayout.addView(newItemLL);
    }
}
