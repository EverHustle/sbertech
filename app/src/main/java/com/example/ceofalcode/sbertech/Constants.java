package com.example.ceofalcode.sbertech;

public class Constants {
    public static final String NEW_FOLDER_PATH = "sdcard/temp";
    public static final int CODE_OK = 200;
    public static final int CODE_FAIL = 405;
    public static final String EXTRA_LIST = "List";
    public static final String FILE_NAME = "daily_currency.xml";
    public static final String FULL_PATH = NEW_FOLDER_PATH + "/" + FILE_NAME;
    public static final String URL = "http://www.cbr.ru/scripts/XML_daily.asp";
}
