package com.example.ceofalcode.sbertech.controller;

import android.content.Context;

import com.example.ceofalcode.sbertech.Constants;
import com.example.ceofalcode.sbertech.model.MainModel;

import org.xmlpull.v1.XmlPullParserException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class MainController {
    private final MainModel _model = new MainModel();


    public void downloadFile(final Context context) throws FileNotFoundException {
        _model.download(context);
    }

    public boolean checkNetwork(Context context) {
        return _model.isNetworkAvailable(context);
    }

    public int createFolder() {
        return _model.createFolder();
    }

    public ArrayList<String> getCurrenciesNameList(Context context) {
        return _model.getCurrenciesNameList(context);
    }

    public ArrayList getDataByName(String currency, Context context) {
        return _model.getDataByName(currency, context);
    }

    public double calculate(double currencyFromET, int initialCurrencyNominal, double initialCurrencyValue, int finiteCurrencyNominal, double finiteCurrencyValue) {
        return _model.calculateCurrency(currencyFromET, initialCurrencyNominal, initialCurrencyValue, finiteCurrencyNominal, finiteCurrencyValue);
    }

    public boolean isFileObtained() {
        return _model.isFileObtained();
    }

    public void parseFile(Context context) throws IOException, XmlPullParserException {
        _model.parseFile(context, Constants.FULL_PATH);
    }
}

