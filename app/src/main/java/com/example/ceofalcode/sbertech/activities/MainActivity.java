package com.example.ceofalcode.sbertech.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ceofalcode.sbertech.Constants;
import com.example.ceofalcode.sbertech.R;
import com.example.ceofalcode.sbertech.controller.MainController;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private final MainController _controller = new MainController();

    private TextView currency_from, currency_to, currency_from_full, currency_to_full, currency_to_TV;
    private EditText currency_from_ET;
    private Button convert;
    private CardView card_to, card_from;

    private static String initialCurrencyCharCode;
    private static int initialCurrencyNominal;
    private static String initialCurrencyName;

    private static double initialCurrencyValue;
    private static String finiteCurrencyCharCode;
    private static int finiteCurrencyNominal;
    private static String finiteCurrencyName;
    private static double finiteCurrencyValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeViewComponents();
        setListeners();
    }


    private void initializeViewComponents() {
        currency_from = (TextView) findViewById(R.id.currency_from);
        currency_to = (TextView) findViewById(R.id.currency_to);
        currency_to_TV = (TextView) findViewById(R.id.currency_to_TV);
        currency_to_full = (TextView) findViewById(R.id.currency_to_full);
        currency_from_full = (TextView) findViewById(R.id.currency_from_full);
        currency_from_ET = (EditText) findViewById(R.id.currency_from_ET);
        convert = (Button) findViewById(R.id.convertBTN);
        card_to = (CardView) findViewById(R.id.cvTo);
        card_from = (CardView) findViewById(R.id.cvFrom);

    }

    private void setListeners() {
        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currency_from.getText().toString().equals("Pick") || currency_to.getText().toString().equals("Pick")) {
                    Toast.makeText(MainActivity.this, getString(R.string.pick_error), Toast.LENGTH_SHORT).show();
                } else {
                    double result = _controller.calculate(getCurrencyFromET(), initialCurrencyNominal, initialCurrencyValue, finiteCurrencyNominal, finiteCurrencyValue);
                    String finalResult = String.format(new Locale("ru"), "%(.2f", result);
                    if (finalResult.length() <= 12) {
                        putResultToTV(finalResult);
                    } else {
                        String tmp = getString(R.string.currency_length_error, currency_from_full.getText().toString());
                        Toast.makeText(MainActivity.this, tmp, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        card_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, getString(R.string.pick_output), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, CurrenciesActivity.class);
                startActivityForResult(intent, 200);
            }
        });
        card_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CurrenciesActivity.class);
                startActivityForResult(intent, 100);
                Toast.makeText(MainActivity.this, getString(R.string.pick_input), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void putResultToTV(String finalResult) {
        initializeViewComponents();
        currency_to_TV.setText(finalResult);
    }

    private double getCurrencyFromET() {
        initializeViewComponents();
        if (!currency_from_ET.getText().toString().equals("")) {
            return Double.parseDouble(currency_from_ET.getText().toString());
        } else {
            String currencyFrom = getString(R.string.empty_input_error, currency_from.getText().toString());
            Toast.makeText(this, currencyFrom, Toast.LENGTH_SHORT).show();
            return 1;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        initializeViewComponents();
        if (data == null)
            return;
        if (requestCode == 100) {
            ArrayList serializedCurrencyData = (ArrayList) data.getSerializableExtra(Constants.EXTRA_LIST);
            for (int i = 0; i < serializedCurrencyData.size(); i++) {
                if ((String.valueOf(serializedCurrencyData.get(i)).length() == 3) && (String.valueOf(serializedCurrencyData.get(i)).matches("^[A-Z]+$"))) {
                    initialCurrencyCharCode = String.valueOf(serializedCurrencyData.get(i));
                }
                if (String.valueOf(serializedCurrencyData.get(i)).length() > 3 && !String.valueOf(serializedCurrencyData.get(i)).contains(".")) {
                    initialCurrencyName = String.valueOf(serializedCurrencyData.get(i));
                }
                if (String.valueOf(serializedCurrencyData.get(i)).contains(".")) {
                    initialCurrencyValue = Double.parseDouble(String.valueOf(serializedCurrencyData.get(i)));
                }
                if (!String.valueOf(serializedCurrencyData.get(i)).contains(".") && String.valueOf(serializedCurrencyData.get(i)).matches("^[0-9]+$")) {
                    initialCurrencyNominal = Integer.parseInt(String.valueOf(serializedCurrencyData.get(i)));
                }
            }

            initializeInitialCurrency(initialCurrencyCharCode, initialCurrencyName);
        }
        if (requestCode == 200) {
            ArrayList serializedCurrencyData = (ArrayList) data.getSerializableExtra(Constants.EXTRA_LIST);
            for (int i = 0; i < serializedCurrencyData.size(); i++) {
                if ((String.valueOf(serializedCurrencyData.get(i)).length() == 3) && (String.valueOf(serializedCurrencyData.get(i)).matches("^[A-Z]+$"))) {
                    finiteCurrencyCharCode = String.valueOf(serializedCurrencyData.get(i));
                }
                if (String.valueOf(serializedCurrencyData.get(i)).length() > 3 && !String.valueOf(serializedCurrencyData.get(i)).contains(".")) {
                    finiteCurrencyName = String.valueOf(serializedCurrencyData.get(i));
                }
                if (String.valueOf(serializedCurrencyData.get(i)).contains(".")) {
                    finiteCurrencyValue = Double.parseDouble(String.valueOf(serializedCurrencyData.get(i)));
                }
                if (!String.valueOf(serializedCurrencyData.get(i)).contains(".") && String.valueOf(serializedCurrencyData.get(i)).matches("^[0-9]+$")) {
                    finiteCurrencyNominal = Integer.parseInt(String.valueOf(serializedCurrencyData.get(i)));
                }
            }

            initializeFiniteCurrency(finiteCurrencyCharCode, finiteCurrencyName);
        }
    }

    private void initializeFiniteCurrency(String finiteCurrencyCharCode, String finiteCurrencyName) {
        initializeViewComponents();
        currency_to.setText(finiteCurrencyCharCode);
        currency_to_full.setText(finiteCurrencyName);

    }

    private void initializeInitialCurrency(String initialCurrencyCharCode, String initialCurrencyName) {
        initializeViewComponents();
        currency_from.setText(initialCurrencyCharCode);
        currency_from_full.setText(initialCurrencyName);

    }
}
