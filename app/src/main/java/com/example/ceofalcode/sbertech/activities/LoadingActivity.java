package com.example.ceofalcode.sbertech.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.ceofalcode.sbertech.Constants;
import com.example.ceofalcode.sbertech.R;
import com.example.ceofalcode.sbertech.controller.MainController;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class LoadingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        try {
            runCheck();
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    private MainController getController() {
        return new MainController();
    }

    private void runCheck() throws IOException, XmlPullParserException {
        if (!getController().checkNetwork(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            if (!getController().isFileObtained()) {
                Toast.makeText(this, getString(R.string.no_internet_no_cache_error), Toast.LENGTH_SHORT).show();
            } else {
                getController().parseFile(getApplicationContext());
                startActivity(new Intent(this, MainActivity.class));
            }
        } else {
            if (getController().createFolder() == Constants.CODE_OK) {
                getController().downloadFile(getApplicationContext());
                while (!getController().isFileObtained()) {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                startActivity(new Intent(this, MainActivity.class));
            } else {
                Toast.makeText(this, getString(R.string.no_permission), Toast.LENGTH_SHORT).show();
            }
        }
    }


}
