package com.example.ceofalcode.sbertech.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.ceofalcode.sbertech.Constants;
import com.example.ceofalcode.sbertech.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class MainModel {
    private final ContentValues cv = new ContentValues();
    private String tmp = "";
    private int count = 0;
    private File new_folder;
    //Database
    private DataBase db;
    private SQLiteDatabase database;

    private void writeToDB(ContentValues cv, Context context) {

        db = new DataBase(context);
        database = db.getWritableDatabase();
        Log.e("write", "Write");
        if (updateDB(context, cv) == 0) {
            database.insert(DataBase.TABLE_NAME, null, cv);
        }
    }

    private int updateDB(Context context, ContentValues cv) {
        Log.e("update", "update");
        db = new DataBase(context);
        database = db.getWritableDatabase();
        String charcode = cv.getAsString(DataBase.COLUMN_CHARCODE);
        return database.update(DataBase.TABLE_NAME, cv, DataBase.COLUMN_CHARCODE + "=" + "?", new String[]{charcode});
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null);

    }

    synchronized public void download(Context context) throws FileNotFoundException {
        DownloadTask downloadTask = new DownloadTask(context);
        downloadTask.execute(Constants.URL);

    }

    public void parseFile(final Context context, final String path) throws IOException, XmlPullParserException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
                    xmlPullParserFactory.setNamespaceAware(true);
                    XmlPullParser xpp = xmlPullParserFactory.newPullParser();
                    File myXML = new File(path);
                    FileInputStream fis = new FileInputStream(myXML);
                    xpp.setInput(fis, null);

                    while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                        switch (xpp.getEventType()) {
                            case XmlPullParser.START_DOCUMENT:
                                break;
                            case XmlPullParser.START_TAG:
                                count = 0;
                                tmp = xpp.getName();
                                break;
                            case XmlPullParser.TEXT:
                                switch (tmp) {
                                    case "CharCode":
                                        String tempCharCode = xpp.getText();
                                        cv.put(DataBase.COLUMN_CHARCODE, tempCharCode);
                                        break;
                                    case "Nominal":
                                        String tempNominal = xpp.getText();
                                        cv.put(DataBase.COLUMN_NOMINAL, tempNominal);
                                        break;
                                    case "Name":
                                        String tempName = xpp.getText();
                                        cv.put(DataBase.COLUMN_NAME, tempName);
                                        break;
                                    case "Value":
                                        String tempValue = (xpp.getText().replace(",", "."));
                                        cv.put(DataBase.COLUMN_VALUE, tempValue);
                                        break;
                                }
                                break;

                            default:
                                break;
                            case XmlPullParser.END_TAG:
                                count++;
                                if (count == 2) {
                                    writeToDB(cv, context);
                                    cv.clear();
                                }
                                tmp = "";
                                break;
                            case XmlPullParser.END_DOCUMENT:

                        }
                        xpp.next();
                    }

                } catch (XmlPullParserException | IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public boolean isFolderExist(File new_folder) {
        return new_folder.exists();
    }

    public boolean isFileObtained() {
        File file = new File(Constants.FULL_PATH);
        return file.exists();
    }

    public int createFolder() {
        new_folder = new File(Constants.NEW_FOLDER_PATH);
        if (!isFolderExist(new_folder)) {
            if (!new_folder.mkdir()) {
                return Constants.CODE_FAIL;
            }
            if (new_folder.mkdir()) {
                return Constants.CODE_OK;
            }
        }

        return Constants.CODE_OK;
    }

    public ArrayList<String> getCurrenciesNameList(Context context) {
        ArrayList<String> list = new ArrayList<>();
        String[] columns = {DataBase.COLUMN_CHARCODE, DataBase.COLUMN_NAME};
        db = new DataBase(context);
        database = db.getWritableDatabase();
        Cursor cursor = database.query(DataBase.TABLE_NAME, columns, null, null, null, null, DataBase.COLUMN_CHARCODE + " ASC");
        while (cursor.moveToNext()) {
            int charcodeIndex = cursor.getColumnIndex(DataBase.COLUMN_CHARCODE);
            int nameIndex = cursor.getColumnIndex(DataBase.COLUMN_NAME);
            String charcode = cursor.getString(charcodeIndex);
            String name = cursor.getString(nameIndex);
            list.add(charcode + " - " + name);
//            Log.e(charcode, " - " + name + "=" + cursor.getPosition());
        }
        cursor.close();
        return list;
    }

    public ArrayList getDataByName(String key, Context context) {
        ArrayList<Object> dataFromSP = new ArrayList<>();
        db = new DataBase(context);
        database = db.getWritableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM " + DataBase.TABLE_NAME + " WHERE " + DataBase.COLUMN_CHARCODE + "= '" + key.trim() + "'", null);
        cursor.moveToFirst();
        int charcodeIndex = cursor.getColumnIndex(DataBase.COLUMN_CHARCODE);
        int nominalIndex = cursor.getColumnIndex(DataBase.COLUMN_NOMINAL);
        int nameIndex = cursor.getColumnIndex(DataBase.COLUMN_NAME);
        int valueIndex = cursor.getColumnIndex(DataBase.COLUMN_VALUE);
        String charcode = cursor.getString(charcodeIndex);
        String name = cursor.getString(nameIndex);
        int nominal = cursor.getInt(nominalIndex);
        double value = cursor.getDouble(valueIndex);
        dataFromSP.add(charcode);
        dataFromSP.add(nominal);
        dataFromSP.add(name);
        dataFromSP.add(value);
        Log.e(charcode, " - " + name + " - " + nominal + " - " + value);
        cursor.close();
        return dataFromSP;
    }

    public double calculateCurrency(double currencyFromET, int initialCurrencyNominal, double initialCurrencyValue, int finiteCurrencyNominal, double finiteCurrencyValue) {
        return (currencyFromET * (initialCurrencyValue / initialCurrencyNominal) / (finiteCurrencyValue / finiteCurrencyNominal));
    }

    private class DownloadTask extends AsyncTask<String, Void, Integer> {

        final Context context;

        DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected Integer doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                URLConnection urlConnection = url.openConnection();
                urlConnection.connect();
                createFolder();
                File currency_list = new File(new_folder, Constants.FILE_NAME);
                InputStream inputStream = new BufferedInputStream(url.openStream(), 8192);
                byte[] data = new byte[1024];
                int count;
                OutputStream outputStream = new FileOutputStream(currency_list);
                while ((count = inputStream.read(data)) != -1) {
                    outputStream.write(data, 0, count);
                }
                inputStream.close();
                outputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return Constants.CODE_OK;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            switch (result) {
                case Constants.CODE_FAIL:
                    Toast.makeText(context, context.getString(R.string.mk_dir_error), Toast.LENGTH_LONG).show();
                    break;
                case Constants.CODE_OK: {
                    String path = Constants.FULL_PATH;
                    try {
                        parseFile(context, path);
                    } catch (IOException | XmlPullParserException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

    }
}
