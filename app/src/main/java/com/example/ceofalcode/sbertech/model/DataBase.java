package com.example.ceofalcode.sbertech.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class DataBase extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "currencies_db";
    private static final int DATABASE_VERSION = 1;
    static final String TABLE_NAME = "currencies";
    private static final String COLUMN_ID = "_id";
    static final String COLUMN_CHARCODE = "charcode";
    static final String COLUMN_NOMINAL = "nominal";
    static final String COLUMN_NAME = "name";
    static final String COLUMN_VALUE = "value";

    DataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                "(" + COLUMN_ID + "INT PRIMARY KEY, AUTO INCREMENT, " +
                COLUMN_CHARCODE + " TEXT, " +
                COLUMN_NOMINAL + " TEXT, " +
                COLUMN_NAME + " TEXT, " +
                COLUMN_VALUE + " TEXT); ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
