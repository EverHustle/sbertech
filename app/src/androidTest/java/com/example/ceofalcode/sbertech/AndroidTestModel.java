package com.example.ceofalcode.sbertech;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.example.ceofalcode.sbertech.model.MainModel;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileNotFoundException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@SuppressWarnings("ResultOfMethodCallIgnored")
@RunWith(AndroidJUnit4.class)
public class AndroidTestModel {

    private Context context;
    private final MainModel _model = new MainModel();


    @Test
    public void testCacheIsNotEmpty() throws Exception {
        this.context = InstrumentationRegistry.getTargetContext();
        _model.download(context);
        Thread.sleep(2000);
//        assertFalse(_model.isFristRun(context));
    }


    @Test
    public void testIsNetworkAvailable() {
        this.context = InstrumentationRegistry.getTargetContext();
        _model.isNetworkAvailable(context);
        assertTrue(_model.isNetworkAvailable(context));
    }

    @Test
    public void testDownload() throws FileNotFoundException {
        this.context = InstrumentationRegistry.getTargetContext();
        _model.download(context);
        File new_folder = new File(Constants.NEW_FOLDER_PATH + "/" + Constants.FILE_NAME);
        assertTrue(_model.isFolderExist(new_folder));
    }

    @Test
    public void testIsFolderExist() {
        this.context = InstrumentationRegistry.getTargetContext();
        File new_folder = new File(Constants.NEW_FOLDER_PATH);
        assertTrue(_model.isFolderExist(new_folder));
    }

    @Test
    public void testCreateFolder() {
        this.context = InstrumentationRegistry.getTargetContext();
        File new_folder = new File(Constants.NEW_FOLDER_PATH);
        new_folder.delete();
        _model.createFolder();
        assertTrue(_model.isFolderExist(new_folder));
    }

    @Test
    public void testCreateFolderIfFolderExist() {
        this.context = InstrumentationRegistry.getTargetContext();
        _model.createFolder();
        assertEquals(Constants.CODE_OK, _model.createFolder());
    }

    @Test
    public void testCreateFolderIfFolderNotExist() {
        this.context = InstrumentationRegistry.getTargetContext();
        File new_folder = new File(Constants.NEW_FOLDER_PATH);
        new_folder.delete();
        assertEquals(Constants.CODE_OK, _model.createFolder());

    }

    @Test
    public void testCalculateCurrency() {
        this.context = InstrumentationRegistry.getTargetContext();
        assertEquals(11.70331588132635, _model.calculateCurrency(150, 100, 134.12, 1, 17.19));
    }
}